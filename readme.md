# A comprehensive approach on installing XDebug in XAMPP with PHP7 and PHPStorm on Windows 10

> #### By Mike213
>
>
> 2019-05-08

## Overview

I have been encountering problems on making XDebug working on my machine for quite a while. I have come across many careless mistakes during the installing process. Though I have googled it, many tutorials seem to repeat something not very important, and some of the configurations are outdated. In order to facilitate the installing process for new users to use PHPStorm, I wrote a concise and precise guide.



## Procedures

- Install XAMPP, start the Apache server.

- go to `htdocs` folder under the XAMPP installation location, and write a `index.php` file containing this line `<?php phpinfo();?>` 

- visit the `localhost/index.php`. select and copy all the information on the webpage, go `https://xdebug.org/wizard.php` and paste to the page. The page will then shows the link for the most appropriate `XDebug.dll` to download.

- Download the DLL and put it into the XAMPP folder, eg: `D:/XAMPP/php/ext`

- modify the `php.ini` in the php folder. *Remark: If you are using `WAMP server`, there are multiple `php.ini` in different folders because of different PHP versions and one in the `apache` folder. It is quite confusing. Therefore I recommend you to use XAMPP. Here is a link for your reference, though this example is in Linux environment*: [Find the correct PHP ini file](https://askubuntu.com/questions/356968/find-the-correct-php-ini-file)

- My configurations of the new contents appended to the `php.ini` are as follows:

  - ```
    [XDebug]
    zend_extension = "D:/XAMPP/php/ext/php_xdebug-2.7.2-7.3-vc15-x86_64.dll"
    xdebug.remote_autostart = 1
    xdebug.profiler_append = 0
    xdebug.profiler_enable = 0
    xdebug.profiler_enable_trigger = 0
    xdebug.profiler_output_dir = "D:/XAMPP/tmp"
    ;xdebug.profiler_output_name = "cachegrind.out.%t-%s"
    xdebug.remote_enable = 1
    xdebug.remote_handler = "dbgp"
    xdebug.remote_host = "127.0.0.1"
    xdebug.remote_log = "D:/XAMPP/tmp/xdebug.txt"
    xdebug.remote_port = 9000
    xdebug.trace_output_dir = "D:/XAMPP/tmp"
    ;36000 = 10h
    xdebug.remote_cookie_expire_time = 36000
    ```

  - Remark:

    - make sure that XDebug has the full read/write/execute priviledge on the `output_dir`, `log` folders. 
    - Change the path as appropriate so that it points to the correct DLL.  **One stupid mistake I made was that I used one forward slash `\` to write the path, and it made apache unable to load the XDebug plugin. Use backward slash instead! **

- Change the Settings in PHPStorm as follows

  - ![](assets/1.PNG)
  - ![](assets/2.PNG)
  - ![](assets/3-1557310428791.PNG)
    - Check the box for path mappings. Otherwise, there will be a chance showing the message that the `PHPStorm is waiting for incoming connection with ide key`
  -  ![](assets/6-1557311101396.PNG)
    - Make sure that the telephone icon on the top right-hand corner should be green for listening to the PHP Debug request
    - Add configuration of a `PHP Webpage` in the `edit configurations` tab under the `Run` dropdown menu on the navigation bar. Remark: make sure it is a `PHP webpage` but not `PHP Script` or `PHP built-in Web Server`.
    - Choose Chrome as the default browser to debug
    - Change the Start URL to the one which is accessible for the browser. For example, I enter the webpage at `index.php`
    - choose the Server that we have constructed in the settings.
    - Before start debugging, click on the `validate ` button to see if all the things are ready. 
    - ![](assets/7.PNG)

- One last step 

  - Install the `XDebug helper` and ` JetBrains IDE Support` extension for Chrome browser.
  - enable the `XDebug helper` to `Debug` mode
  - Set the `IDE key` in the `XDebug helper` to be `PHPStorm`
  - ![](assets/8.PNG)

- Now, go back to your PHPStorm, and press the `Debug` button. Enjoy~~~

  - ![](assets/9.PNG)



